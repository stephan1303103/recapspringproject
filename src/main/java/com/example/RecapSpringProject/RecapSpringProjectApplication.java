package com.example.RecapSpringProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecapSpringProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecapSpringProjectApplication.class, args);
	}

}
