package com.example.RecapSpringProject.service;

import com.example.RecapSpringProject.entity.Person;
import com.example.RecapSpringProject.repository.PersonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    public List<Person> getAllPersons(){
        return  personRepository.findAll();
    }

    public void createPerson(Person person){
        personRepository.save(person);
    }


}
