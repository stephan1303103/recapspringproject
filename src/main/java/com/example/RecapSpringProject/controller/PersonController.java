package com.example.RecapSpringProject.controller;

import com.example.RecapSpringProject.entity.Person;
import com.example.RecapSpringProject.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value="/person")
public class PersonController {
    @Autowired
    private PersonService personService;

    @GetMapping("/")
    public String getAllPersons(@ModelAttribute Person person, Model model){
        List<Person> persons= personService.getAllPersons();
        model.addAttribute("persons", persons);
        return "index";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute Person person, BindingResult errors, Model model){
        personService.createPerson(person);
        model.addAttribute("persons", personService.getAllPersons());
        return "index";
    }


}
